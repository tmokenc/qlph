package qlph;
/* @Nhom 8
 * @author TRAN QUOC THAI AN & THAI TRUONG HONG PHUC
 */
public class PH {
        protected String maphong;
	protected String daynha;
	protected long dientich;
        protected long sobongden;
	
	public PH(){
		this.maphong = "";
		this.daynha = "";
		this.dientich = 0;
                this.sobongden = 0;
	}

    public PH(String maphong, String daynha, long dientich, long sobongden) {
    	this.maphong = maphong;
    	this.daynha = daynha;
    	this.dientich = dientich;
        this.sobongden = sobongden;
    }
    
    public String getMP() {
    	return this.maphong;
    }
    
    public String getDN() {
    	return this.daynha;
    }
    
    public long getDT() {
    	return this.dientich;
    }
    
    public long getSBD() {
        return this.sobongden;
    }
    
    public void setMP(String maphong) {
    	this.maphong = maphong;
    }
    
    public void setDN(String daynha) {
    	this.daynha = daynha;
    }
    
    public void setDT(long dientich) {
    	this.dientich = dientich;
    }
    
    public void setSBD(long sobongden) {
        this.sobongden = sobongden;
    }
    
    public void nhap() {
    }
    
    public void xuat() {
    }
}
