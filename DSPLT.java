package qlph;
import java.util.Scanner;
public class DSPLT {
    private PH[] DSPLT;
    private int soLuong;

    public DSPLT()
    {
    	this.DSPLT = null;
    	this.soLuong = 0;
    }
    
	public void nhap()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Nhap so PLT: ");
		this.soLuong = scanner.nextInt();
		
		this.DSPLT = new PLT[this.soLuong];
		
		System.out.println("\n\nNhap danh sach PLT: ");
		
		for (int i = 0; i < this.DSPLT.length; i++) {
			System.out.printf("Phong ly thuyet so %d:\n", i + 1);
			
			PH PLT = new PLT();
			PLT.nhap();
			
			this.DSPLT[i] = PLT;
			
			System.out.println();
		}
	}
	
	public void xuat() {
		System.out.println("\n\nXuat danh sach: ");
		
		for (int i = 0; i < this.DSPLT.length; i++) {
			System.out.printf("Phong ly thuyet so %d:\n", i + 1);
			
			this.DSPLT[i].xuat();
			
			System.out.println();
		}
	}
}
