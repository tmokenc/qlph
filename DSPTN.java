package qlph;
import java.util.Scanner;
public class DSPTN {
    private PH[] DSPTN;
    private int soLuong;

    public DSPTN()
    {
    	this.DSPTN = null;
    	this.soLuong = 0;
    }
    
	public void nhap()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Nhap so PTN: ");
		this.soLuong = scanner.nextInt();
		
		this.DSPTN = new PTN[this.soLuong];
		
		System.out.println("\n\nNhap danh sach PTN: ");
		
		for (int i = 0; i < this.DSPTN.length; i++) {
			System.out.printf("Phong thi nghiem so %d:\n", i + 1);
			
			PH PTN = new PTN();
			PTN.nhap();
			
			this.DSPTN[i] = PTN;
			
			System.out.println();
		}
	}
	
	public void xuat() {
		System.out.println("\n\nXuat danh sach: ");
		
		for (int i = 0; i < this.DSPTN.length; i++) {
			System.out.printf("Phong thi nghiem so %d:\n", i + 1);
			
			this.DSPTN[i].xuat();
			
			System.out.println();
		}
	}
}
