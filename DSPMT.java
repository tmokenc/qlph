package qlph;
import java.util.Scanner;
public class DSPMT {
    private PH[] DSPMT;
    private int soLuong;

    public DSPMT()
    {
    	this.DSPMT = null;
    	this.soLuong = 0;
    }
    
	public void nhap()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Nhap so PMT: ");
		this.soLuong = scanner.nextInt();
		
		this.DSPMT = new PMT[this.soLuong];
		
		System.out.println("\n\nNhap danh sach PMT: ");
		
		for (int i = 0; i < this.DSPMT.length; i++) {
			System.out.printf("Phong may tinh so %d:\n", i + 1);
			
			PH PMT = new PMT();
			PMT.nhap();
			
			this.DSPMT[i] = PMT;
			
			System.out.println();
		}
	}
	
	public void xuat() {
		System.out.println("\n\nXuat danh sach: ");
		
		for (int i = 0; i < this.DSPMT.length; i++) {
			System.out.printf("Phong may tinh so %d:\n", i + 1);
			
			this.DSPMT[i].xuat();
			
			System.out.println();
		}
	}
}
