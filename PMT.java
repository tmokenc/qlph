package qlph;
/* @Nhom 8
 * @author TRAN QUOC THAI AN & THAI TRUONG HONG PHUC
 */
import java.util.Scanner;

public class PMT extends PH{
        private int maytinh;

	public PMT()
	{
		super();
		this.maytinh = 0;
	}

    public PMT(String maphong, String daynha, long dientich, long sobongden, int maytinh)
    {
    	super(maphong, daynha, dientich, sobongden);
    	this.maytinh = maytinh;
    }

    public int getmaytinh()
    {
    	return this.maytinh;
    }

    public void setmaytinh(int maytinh)
    {
    	this.maytinh = maytinh;
    }
    
    public void nhap()
    {
    	Scanner scanner = new Scanner(System.in);

    	System.out.print("Nhap ma phong: ");
    	this.maphong = scanner.nextLine();

    	System.out.print("Nhap day nha: ");
    	this.daynha = scanner.nextLine();

    	System.out.print("Nhap dien tich [m2]: ");
    	this.dientich = scanner.nextLong();
        
        System.out.print("Nhap so bong den: ");
    	this.sobongden = scanner.nextLong();

    	scanner.nextLine();

    	System.out.print("Nhap so bo may tinh: ");
    	this.maytinh = scanner.nextInt();
    	
    }

    public void xuat()
    {
    	System.out.println("Ma phong: " + this.maphong);
    	System.out.println("Day nha: " + this.daynha);
    	System.out.println("Dien tich: " + this.dientich + "m2");
        System.out.println("So bong den: " + this.sobongden);
    	System.out.println("So bo may tinh: " + this.maytinh);
        if(this.maytinh > 0)
            System.out.println("Phong may tinh dat chuan");
        else
            System.out.println("Phong may tinh khong dat chuan");
    }
}
