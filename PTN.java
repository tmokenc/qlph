package qlph;
/* @Nhom 8
 * @author TRAN QUOC THAI AN & THAI TRUONG HONG PHUC
 */
import java.util.*;
import java.io.*;

public class PTN extends PH{
        private String chuyennganh;
        private int succhua;
        private int bonrua;

	public PTN()
	{
		super();
                this.chuyennganh = "";
		this.succhua = 0;
                this.bonrua = 0;
	}

    public PTN(String maphong, String daynha, long dientich, long sobongden, String chuyennganh, int succhua, int bonrua)
    {
    	super(maphong, daynha, dientich, sobongden);
        this.chuyennganh = chuyennganh;
    	this.succhua = succhua;
        this.bonrua = bonrua;
    }

    public String getchuyennganh()
    {
    	return this.chuyennganh;
    }
    
    public int getsucchua()
    {
    	return this.succhua;
    }
    
    public int getbonrua()
    {
        return this.bonrua;
    }

    public void setchuyennganh(String chuyennganh)
    {
    	this.succhua = succhua;
    }
    
    public void setsucchua(int succhua)
    {
    	this.succhua = succhua;
    }
    
    public void setbonrua(int bonrua)
    {
    	this.bonrua = bonrua;
    }
    
    public void nhap()
    {
    	Scanner scanner = new Scanner(System.in);

    	System.out.print("Nhap ma phong: ");
    	this.maphong = scanner.nextLine();

    	System.out.print("Nhap day nha: ");
    	this.daynha = scanner.nextLine();

    	System.out.print("Nhap dien tich [m2]: ");
    	this.dientich = scanner.nextLong();
        
        System.out.print("Nhap so bong den: ");
    	this.sobongden = scanner.nextLong();

    	scanner.nextLine();

    	System.out.print("Nhap so bon rua: ");
    	this.bonrua = scanner.nextInt();
    	
    }

    public void xuat()
    {
    	System.out.println("Ma phong: " + this.maphong);
    	System.out.println("Day nha: " + this.daynha);
    	System.out.println("Dien tich: " + this.dientich + "m2");
        System.out.println("So bong den: " + this.sobongden);
    	System.out.println("So bon rua: " + this.bonrua);
        if(this.bonrua > 0)
            System.out.println("Phong thi nghiem dat chuan");
        else
            System.out.println("Phong thi nghiem khong dat chuan");
    }
}
