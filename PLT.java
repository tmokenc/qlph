package qlph;
/* @Nhom 8
 * @author TRAN QUOC THAI AN & THAI TRUONG HONG PHUC
 */
import java.util.Scanner;

public class PLT extends PH{
        private int maychieu;

	public PLT()
	{
		super();
		this.maychieu = 0;
	}

    public PLT(String maphong, String daynha, long dientich, long sobongden, int maychieu)
    {
    	super(maphong, daynha, dientich, sobongden);
    	this.maychieu = maychieu;
    }

    public int getmaychieu()
    {
    	return this.maychieu;
    }

    public void setmaychieu(int maychieu)
    {
    	this.maychieu = maychieu;
    }
    
    public void nhap()
    {
    	Scanner scanner = new Scanner(System.in);

    	System.out.print("Nhap ma phong: ");
    	this.maphong = scanner.nextLine();

    	System.out.print("Nhap day nha: ");
    	this.daynha = scanner.nextLine();

    	System.out.print("Nhap dien tich [m2]: ");
    	this.dientich = scanner.nextLong();
        
        System.out.print("Nhap so bong den: ");
    	this.sobongden = scanner.nextLong();

    	scanner.nextLine();

    	System.out.print("Nhap so may chieu: ");
    	this.maychieu = scanner.nextInt();
    	
    }

    public void xuat()
    {
    	System.out.println("Ma phong: " + this.maphong);
    	System.out.println("Day nha: " + this.daynha);
    	System.out.println("Dien tich: " + this.dientich + "m2");
        System.out.println("So bong den: " + this.sobongden);
    	System.out.println("So may chieu: " + this.maychieu);
        if(this.maychieu > 0)
            System.out.println("Phong ly thuyet dat chuan");
        else
            System.out.println("Phong ly thuyet khong dat chuan");
    }
}
